# -*- coding: utf-8 -*-
from odoo import tools
from odoo import api, fields, models


class TodoReport(models.Model):
    _name = 'hr.employee.report'
    _description = 'Report'
    #_auto = False

    # _sql = """
    #         CREATE OR REPLACE VIEW hr_employee_report AS
    #         SELECT hr_employee.name, hr_employee.job_id, hr_employee.department_id, hr_employee.gender,
    #         pegawai_formal.nama_sekolah_formal
    #         FROM hr_employee, pegawai_formal
    #         INNER JOIN pegawai_formal AS pegawai_formal_report
    #         ON hr_employee.id = pegawai_formal.id_pegawai
    #         WHERE active = True
    #     """
    #
    # name = fields.Char('Description')
    # job_id = fields.Char('Job')
    # department_id = fields.Char('Departement?')
    # gender = fields.Char('JK')
    # npp = fields.Char('NPP')
    # formal_diri = fields.One2many('pegawai.formal', 'id_pegawai', string='Pendidikan Formal', readonly=False)
    # id_pegawai = fields.Many2one('hr.employee', 'Nama')
