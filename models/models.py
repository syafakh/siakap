# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions

# class asli(models.Model):
#     _name = 'asli.asli'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class hr_employee(models.Model):
    _inherit = 'hr.employee'

    # _sql_constraints = [
    #     ('npp_uniq', 'UNIQUE(npp)', 'NPP tidak boleh ada yang sama')
    # ]

    npp = fields.Char(string='NPP', required=True)
    no_identitas = fields.Char(string='KTP No')
    place_birthday = fields.Char(string='Tempat Lahir')
    status_peg = fields.Selection([
        ('PTT', 'PTT'),
        ('LS', 'LS'),
        ('PT', 'LS')], string='Status')
    agama_peg = fields.Selection([
        ('Islam', 'Islam'),
        ('Kristen', 'Kristen'),
        ('Kristen Protestan', 'Kristen Protestan'),
        ('Katolik', 'Katolik'),
        ('Budha', 'Budha'),
        ('Hindu', 'Hindu')], string="Agama")
    gol_darah_peg = fields.Selection([
        ('A', 'A'),
        ('B', 'B'),
        ('O', 'O'),
        ('AB', 'AB')], string="Golongan Darah")
    tanggal_masuk_peg = fields.Date(string="Tanggal Masuk")
    tanggal_diangkat_peg = fields.Date(string="Tanggal Diangkat")
    pensiun_peg = fields.Date(string="Pensiun")
    # jabatan_saat_ini_peg = fields.Char(string="Jabatan Saat Ini")
    no_hp_peg = fields.Char(string="No HP")
    unit_kerja = fields.Char(string="Unit Kerja")
    alamat_peg = fields.Text(string="Alamat")
    rt_peg = fields.Char(string="RT")
    rw_peg = fields.Char(string="RW")
    kelurahan_peg = fields.Char(string="Kelurahan")
    kecamatan_peg = fields.Char(string="Kecamatan")
    kota_peg = fields.Char(string="Kota")
    kode_pos = fields.Char(string="Kode Pos")
    jabatan_sejak_peg = fields.Date(string="Jabatan Sejak")
    pangkat_sejak_peg = fields.Date(string="Pangkat Sejak")
    tingkat_pang_peg = fields.Selection([
        ('1A', '1A'),
        ('1B', '1B'),
        ('1C', '1C'),
        ('1D', '1D'),
        ('2A', '2A'),
        ('2B', '2B'),
        ('2C', '2C'),
        ('2D', '2D'),
        ('3A', '3A'),
        ('3B', '3B'),
        ('3C', '3C'),
        ('3D', '3D'),
        ('4A', '4A'),
        ('4B', '4B'),
        ('4C', '4C'),
        ('4D', '4D'),
        ('5A', '5A'),
        ('5B', '5B'),
        ('5C', '5C'),
        ('5D', '5D'),
        ('6A', '6A'),
        ('6B', '6B'),
        ('6C', '6C'),
        ('6D', '6D'),
        ('7A', '7A'),
        ('7B', '7B'),
        ('7C', '7C'),
        ('7D', '7D')], string="Tingkat Pangkat")
    tingkat_jab_peg = fields.Selection([
        ('2A', '2A'),
        ('2B', '2B'),
        ('2C', '2C'),
        ('2D', '2D'),
        ('3A', '3A'),
        ('3B', '3B'),
        ('3C', '3C'),
        ('4A', '4A'),
        ('4B', '4B'),
        ('4C', '4C'),
        ('4D', '4D'),
        ('5A', '5A'),
        ('5B', '5B'),
        ('6A', '6A'),
        ('6B', '6B'),
        ('6C', '6C')], string="Tingkat Jabatan")



    keluarga_diri = fields.One2many('pegawai.keluarga', 'id_pegawai', string='Keluarga', readonly=False)
    formal_diri = fields.One2many('pegawai.formal', 'id_pegawai', string='Pendidikan Formal', readonly=False)
    nonformal_diri = fields.One2many('pegawai.nonformal', 'id_pegawai', string='Pendidikan Non Formal', readonly=False)
    riwayat_diri = fields.One2many('pegawai.riwayat', 'id_pegawai', string='Riwayat Pendidikan', readonly=False)

class pegawai_keluarga(models.Model):
    _name = 'pegawai.keluarga'

    id_pegawai = fields.Many2one('hr.employee', string='Nama', required=True)
    nama_kel = fields.Char(string="Nama Keluarga", required=True)
    pekerjaan_kel = fields.Char(string="Pekerjaan")
    pendidikan_kel = fields.Selection([
        ('SD', 'SD'),
        ('SMP', 'SMP'),
        ('SMA', 'SMA'),
        ('S1', 'S1'),
        ('S2', 'S2'),
        ('S3', 'S3')], string="Pendidikan")
    hubungan_kel = fields.Selection([
        ('Ibu', 'Ibu'),
        ('Bapak', 'Bapak'),
        ('Istri/Suami', 'Istri/Suami'),
        ('Anak', 'Anak')], string="Hubungan")
    jen_kel = fields.Selection([
        ('Laki-laki', 'Laki-laki'),
        ('Perempuan', 'Perempuan')], string='Jenis Kelamin')
    tanggal_lahir_kel = fields.Date(string="Tanggal Lahir")
    tempat_lahir_kel = fields.Char(string="Tempat Lahir")
    tanggal_wafat_kel = fields.Date(string="Tanggal Wafat")

class pegawai_formal(models.Model):
    _name = 'pegawai.formal'

    id_pegawai = fields.Many2one('hr.employee', string='Nama', required=True)
    nama_sekolah_formal = fields.Char(string="Nama Sekolah", required=True)
    tingkat_pendidikan_formal = fields.Selection([
        ('SD', 'SD'),
        ('SMP', 'SMP'),
        ('SMA', 'SMA'),
        ('S1', 'S1'),
        ('S2', 'S2'),
        ('S3', 'S3')], string="Tingkat Pendidikan")
    kota_formal = fields.Char(string="Kota")
    tahun_lulus_formal = fields.Char(string="Tahun Lulus")
    tanggal_ijazah_formal = fields.Date(string="Tanggal Ijazah")

class pegawai_nonformal(models.Model):
    _name ='pegawai.nonformal'



    id_pegawai = fields.Many2one('hr.employee', string='Nama', required=True)
    nama_pel_nonformal = fields.Char(string="Nama Pelatihan", required=True)
    penyelenggara_nonformal = fields.Char(string="Penyelenggara")
    no_nonformal = fields.Char(string="No Sertifikat")
    mulai_nonformal = fields.Date(string="Mulai")
    selesai_nonformal = fields.Date(string="Selesai")
    tanggal_berlaku_nonformal = fields.Date(string="Berlaku")
    sampai_nonformal = fields.Date(string="Sampai")


    percentage = fields.Integer(string="Jumlah Data", compute='_get_nama_pel_count', store=True)


    @api.depends('nama_pel_nonformal')
    def _get_nama_pel_count(self):
        for r in self:
            # Mengupdate field percentage berdasarkan jumlah record di tabel peserta
            r.percentage = len(r.nama_pel_nonformal)


class pegawai_riwayat(models.Model):
    _name = 'pegawai.riwayat'

    id_pegawai = fields.Many2one('hr.employee', string='Nama', required=True)
    posisi_riw = fields.Char(string="Posisi", required=True)
    tingkat_riw = fields.Selection([
        ('2A', '2A'),
        ('2B', '2B'),
        ('2C', '2C'),
        ('2D', '2D'),
        ('3A', '3A'),
        ('3B', '3B'),
        ('3C', '3C'),
        ('4A', '4A'),
        ('4B', '4B'),
        ('4C', '4C'),
        ('4D', '4D'),
        ('5A', '5A'),
        ('5B', '5B'),
        ('6A', '6A'),
        ('6B', '6B'),
        ('6C', '6C')], string="Tingkat")
    pangkat_sejak_riw = fields.Date(string="Pangkat Sejak")
    lokasi_riw = fields.Char(string="Lokasi")
    mulai_riw = fields.Date(string="Mulai")
    no_sk_riw = fields.Char(string="No SK")
    tgl_sk_riw = fields.Date(string="Tanggal SK")
