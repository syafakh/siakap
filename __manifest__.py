# -*- coding: utf-8 -*-
{
    'name': "Sistem Informasi Kepakaran Pegawai",

    'summary': """
        Mengelola Data Pegawai""",

    'description': """
        Data Pegawai PT SUCOFINDO Cabang Semarang
    """,

    'author': "Syafaatun Khasanah",
    'website': "http://www.sucofindo.co.id",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Data Internal',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'reports/reportcv.xml',
        'reports/reportjabatan.xml',
        'reports/reportformal.xml',
        'reports/reportnonformal.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
